//! The tokenizer for the ezi language, converting source to tokens.
//!
//! This is heavily inspired from the tokenizer in rustpython,
//! https://github.com/RustPython/RustPython/blob/master/parser/src/lexer.rs

pub use super::token::Tok;
use crate::location::Location;

use crate::error::{LexicalError, LexicalErrorType};

use std::collections::HashMap;

use std::str::FromStr;
use unic_ucd_ident::{is_xid_continue, is_xid_start};


pub struct Lexer<T: Iterator<Item = char>> {
    chars: T,
    nesting: usize,
    pending: Vec<Spanned>,
    chr0: Option<char>,
    chr1: Option<char>,
    chr2: Option<char>,
    location: Location,
    keywords: HashMap<String, Tok>,
}

pub type Spanned = (Location, Tok, Location);
pub type LexResult = Result<Spanned, LexicalError>;

fn get_keywords() -> HashMap<String, Tok> {
    let mut keywords: HashMap<String, Tok> = HashMap::new();

    // Alphabetical keywords:
    keywords.insert(String::from("interface"), Tok::Interface);
    keywords.insert(String::from("node"), Tok::Node);
    keywords.insert(String::from("struct"), Tok::Struct);
    keywords.insert(String::from("import"), Tok::Import);
    keywords.insert(String::from("enum"), Tok::Enum);
    keywords.insert(String::from("variant"), Tok::Variant);

    keywords.insert(String::from("int"), Tok::TypeInt);
    keywords.insert(String::from("uint"), Tok::TypeUint);
    keywords.insert(String::from("bool"), Tok::TypeBool);
    keywords.insert(String::from("string"), Tok::TypeString);
    keywords.insert(String::from("double"), Tok::TypeDouble);
    keywords.insert(String::from("list"), Tok::TypeList);
    keywords.insert(String::from("map"), Tok::TypeMap);
    keywords.insert(String::from("optional"), Tok::TypeOptional);
    keywords.insert(String::from("refvalue"), Tok::TypeRefValue);

    keywords.insert(String::from("params"), Tok::Params);
    keywords.insert(String::from("public_ref"), Tok::PublicRef);
    keywords.insert(String::from("true"), Tok::True);
    keywords.insert(String::from("false"), Tok::False);
    keywords.insert(String::from("properties"), Tok::Properties);

    keywords
}

pub fn make_tokenizer<'a>(source: &'a str) -> impl Iterator<Item = LexResult> + 'a {
    Lexer::new(source.chars())
}

impl<T> Lexer<T>
where
    T: Iterator<Item = char>,
{
    pub fn new(input: T) -> Self {
        let mut lxr = Lexer {
            chars: input,
            nesting: 0,
            pending: Vec::new(),
            chr0: None,
            location: Location::new(0, 0),
            chr1: None,
            chr2: None,
            keywords: get_keywords(),
        };
        lxr.next_char();
        lxr.next_char();
        lxr.next_char();
        // Start at top row (=1) left column (=1)
        lxr.location.reset();
        lxr
    }

    // Lexer helper functions:
    fn lex_identifier(&mut self) -> LexResult {
        let mut name = String::new();
        let start_pos = self.get_pos();

        while self.is_identifier_continuation() {
            name.push(self.next_char().unwrap());
        }
        let end_pos = self.get_pos();

        if self.keywords.contains_key(&name) {
            Ok((start_pos, self.keywords[&name].clone(), end_pos))
        } else {
            Ok((start_pos, Tok::Name(name), end_pos))
        }
    }

    /// Numeric lexing. The feast can start!
    fn lex_number(&mut self) -> LexResult {
        let start_pos = self.get_pos();
        if self.chr0 == Some('0') {
            if self.chr1 == Some('x') || self.chr1 == Some('X') {
                // Hex!
                self.next_char();
                self.next_char();
                self.lex_number_radix(start_pos, 16)
            } else if self.chr1 == Some('o') || self.chr1 == Some('O') {
                // Octal style!
                self.next_char();
                self.next_char();
                self.lex_number_radix(start_pos, 8)
            } else if self.chr1 == Some('b') || self.chr1 == Some('B') {
                // Binary!
                self.next_char();
                self.next_char();
                self.lex_number_radix(start_pos, 2)
            } else {
                self.lex_normal_number()
            }
        } else {
            self.lex_normal_number()
        }
    }

    /// Lex a hex/octal/decimal/binary number without a decimal point.
    fn lex_number_radix(&mut self, start_pos: Location, radix: u32) -> LexResult {
        let value_text = self.radix_run(radix);
        let end_pos = self.get_pos();
        let value = i64::from_str_radix(&value_text, radix).map_err(|e| LexicalError {
            error: LexicalErrorType::OtherError(format!("{:?}", e)),
            location: start_pos.clone(),
        })?;
        Ok((start_pos, Tok::Int(value), end_pos))
    }

    /// Lex a normal number, that is, no octal, hex or binary number.
    fn lex_normal_number(&mut self) -> LexResult {
        let start_pos = self.get_pos();
        // Normal number:
        let mut value_text = self.radix_run(10);

        // If float:
        if (self.chr0 == Some('.')
            // if two dots, this might be a DotDot
            && self.chr1 != Some('.')) || self.at_exponent() {
            // Take '.':
            if self.chr0 == Some('.') {
                if self.chr1 == Some('_') {
                    return Err(LexicalError {
                        error: LexicalErrorType::OtherError("Invalid Syntax".to_string()),
                        location: self.get_pos(),
                    });
                }
                value_text.push(self.next_char().unwrap());
                value_text.push_str(&self.radix_run(10));
            }

            // 1e6 for example:
            if self.chr0 == Some('e') || self.chr0 == Some('E') {
                value_text.push(self.next_char().unwrap().to_ascii_lowercase());

                // Optional +/-
                if self.chr0 == Some('-') || self.chr0 == Some('+') {
                    value_text.push(self.next_char().unwrap());
                }

                value_text.push_str(&self.radix_run(10));
            }

            let value = f64::from_str(&value_text).unwrap();


            let end_pos = self.get_pos();
            Ok((start_pos, Tok::Double(value), end_pos))
        } else {
            let end_pos = self.get_pos();
            let value = value_text.parse::<i64>().unwrap();
            Ok((start_pos, Tok::Int(value), end_pos))
        }
    }

    /// Consume a sequence of numbers with the given radix,
    /// the digits can be decorated with underscores
    /// like this: '1_2_3_4' == '1234'
    fn radix_run(&mut self, radix: u32) -> String {
        let mut value_text = String::new();

        loop {
            if let Some(c) = self.take_number(radix) {
                value_text.push(c);
            } else if self.chr0 == Some('_') && Lexer::<T>::is_digit_of_radix(self.chr1, radix) {
                self.next_char();
            } else {
                break;
            }
        }
        value_text
    }

    /// Consume a single character with the given radix.
    fn take_number(&mut self, radix: u32) -> Option<char> {
        let take_char = Lexer::<T>::is_digit_of_radix(self.chr0, radix);

        if take_char {
            Some(self.next_char().unwrap())
        } else {
            None
        }
    }

    /// Test if a digit is of a certain radix.
    fn is_digit_of_radix(c: Option<char>, radix: u32) -> bool {
        match radix {
            2 => match c {
                Some('0'..='1') => true,
                _ => false,
            },
            8 => match c {
                Some('0'..='7') => true,
                _ => false,
            },
            10 => match c {
                Some('0'..='9') => true,
                _ => false,
            },
            16 => match c {
                Some('0'..='9') | Some('a'..='f') | Some('A'..='F') => true,
                _ => false,
            },
            x => unimplemented!("Radix not implemented: {}", x),
        }
    }

    /// Test if we face '[eE][-+]?[0-9]+'
    fn at_exponent(&self) -> bool {
        match self.chr0 {
            Some('e') | Some('E') => match self.chr1 {
                Some('+') | Some('-') => match self.chr2 {
                    Some('0'..='9') => true,
                    _ => false,
                },
                Some('0'..='9') => true,
                _ => false,
            },
            _ => false,
        }
    }

    /// Skip everything until end of line
    fn lex_comment(&mut self) {
        self.next_char();
        loop {
            match self.chr0 {
                Some('\n') => return,
                Some(_) => {}
                None => return,
            }
            self.next_char();
        }
    }

    fn lex_string(&mut self) -> LexResult {
        let quote_char = self.next_char().unwrap();
        let mut string_content = String::new();
        let start_pos = self.get_pos();

        // If the next two characters are also the quote character, then we have a triple-quoted
        // string; consume those two characters and ensure that we require a triple-quote to close
        let triple_quoted = if self.chr0 == Some(quote_char) && self.chr1 == Some(quote_char) {
            self.next_char();
            self.next_char();
            true
        } else {
            false
        };

        loop {
            match self.next_char() {
                Some('\\') => {
                    if self.chr0 == Some(quote_char) {
                        string_content.push(quote_char);
                        self.next_char();
                    } else {
                        match self.next_char() {
                            Some('\\') => {
                                string_content.push('\\');
                            }
                            Some('\'') => string_content.push('\''),
                            Some('\"') => string_content.push('\"'),
                            Some('\n') => {
                                // Ignore Unix EOL character
                            }
                            Some('a') => string_content.push('\x07'),
                            Some('b') => string_content.push('\x08'),
                            Some('f') => string_content.push('\x0c'),
                            Some('n') => {
                                string_content.push('\n');
                            }
                            Some('r') => string_content.push('\r'),
                            Some('t') => {
                                string_content.push('\t');
                            }
                            Some(c) => {
                                string_content.push('\\');
                                string_content.push(c);
                            }
                            None => {
                                return Err(LexicalError {
                                    error: LexicalErrorType::StringError,
                                    location: self.get_pos(),
                                });
                            }
                        }
                    }
                }
                Some(c) => {
                    if c == quote_char {
                        if triple_quoted {
                            // Look ahead at the next two characters; if we have two more
                            // quote_chars, it's the end of the string; consume the remaining
                            // closing quotes and break the loop
                            if self.chr0 == Some(quote_char) && self.chr1 == Some(quote_char) {
                                self.next_char();
                                self.next_char();
                                break;
                            }
                            string_content.push(c);
                        } else {
                            break;
                        }
                    } else {
                        if c == '\n' && !triple_quoted {
                            return Err(LexicalError {
                                error: LexicalErrorType::StringError,
                                location: self.get_pos(),
                            });
                        }
                        string_content.push(c);
                    }
                }
                None => {
                    return Err(LexicalError {
                        error: LexicalErrorType::StringError,
                        location: self.get_pos(),
                    });
                }
            }
        }
        let end_pos = self.get_pos();
        let tok = Tok::String(string_content);

        Ok((start_pos, tok, end_pos))
    }

    fn is_identifier_start(&self, c: char) -> bool {
        c == '_' || is_xid_start(c)
    }

    fn is_identifier_continuation(&self) -> bool {
        if let Some(c) = self.chr0 {
            match c {
                '_' | '0'..='9' => true,
                c => is_xid_continue(c),
            }
        } else {
            false
        }
    }

    /// This is the main entry point. Call this function to retrieve the next token.
    /// This function is used by the iterator implementation.
    fn inner_next(&mut self) -> LexResult {
        // top loop, keep on processing, until we have something pending.
        while self.pending.is_empty() {
            self.consume_normal()?;
        }

        Ok(self.pending.remove(0))
    }

    /// Take a look at the next character, if any, and decide upon the next steps.
    fn consume_normal(&mut self) -> Result<(), LexicalError> {
        // Check if we have some character:
        if let Some(c) = self.chr0 {
            // First check identifier:
            if self.is_identifier_start(c) {
                let identifier = self.lex_identifier()?;
                self.emit(identifier);
            } else {
                self.consume_character(c)?;
            }
        } else {
            // We reached end of file.
            let tok_pos = self.get_pos();

            // First of all, we need all nestings to be finished.
            if self.nesting > 0 {
                return Err(LexicalError {
                    error: LexicalErrorType::NestingError,
                    location: tok_pos,
                });
            }

            self.emit((tok_pos.clone(), Tok::EndOfFile, tok_pos));
        }

        Ok(())
    }

    /// Okay, we are facing a weird character, what is it? Determine that.
    fn consume_character(&mut self, c: char) -> Result<(), LexicalError> {
        match c {
            '0'..='9' => {
                let number = self.lex_number()?;
                self.emit(number);
            }
            '"' | '\'' => {
                let string = self.lex_string()?;
                self.emit(string);
            }
            '/' => {
                let tok_start = self.get_pos();
                self.next_char();
                match self.chr0 {
                    Some('/') => {
                        self.lex_comment();
                    }
                    _ => {
                        return Err(LexicalError {
                            error: LexicalErrorType::UnrecognizedToken { tok: '/' },
                            location: tok_start,
                        });
                    }
                }
            }
            '[' => {
                self.eat_single_char(Tok::Lsqb);
                self.nesting += 1;
            }
            ']' => {
                self.eat_single_char(Tok::Rsqb);
                if self.nesting == 0 {
                    return Err(LexicalError {
                        error: LexicalErrorType::NestingError,
                        location: self.get_pos(),
                    });
                }
                self.nesting -= 1;
            }
            '{' => {
                self.eat_single_char(Tok::Lbrace);
                self.nesting += 1;
            }
            '}' => {
                self.eat_single_char(Tok::Rbrace);
                if self.nesting == 0 {
                    return Err(LexicalError {
                        error: LexicalErrorType::NestingError,
                        location: self.get_pos(),
                    });
                }
                self.nesting -= 1;
            }
            ':' => {
                self.eat_single_char(Tok::Colon);
            }
            ';' => {
                self.eat_single_char(Tok::SemiColon);
            }
            '<' => {
                self.eat_single_char(Tok::Lshift);
            }
            '>' => {
                self.eat_single_char(Tok::Rshift);
            }
            '=' => {
                self.eat_single_char(Tok::Equal);
            }
            '-' => {
                self.eat_single_char(Tok::Minus);
            }
            ',' => {
                let tok_start = self.get_pos();
                self.next_char();
                let tok_end = self.get_pos();
                self.emit((tok_start, Tok::Comma, tok_end));
            }
            '.' => {
                if let Some('0'..='9') = self.chr1 {
                    let number = self.lex_number()?;
                    self.emit(number);
                } else {
                    let tok_start = self.get_pos();
                    let token = if Some('.') == self.chr1 {
                        self.next_char();
                        self.next_char();
                        Tok::DotDot
                    } else {
                        self.next_char();
                        Tok::Dot
                    };
                    let tok_end = self.get_pos();
                    self.emit((tok_start, token, tok_end));
                }
            }
            ' ' | '\t' | '\x0C' | '\n' => {
                // Skip whitespaces
                self.next_char();
                while self.chr0 == Some(' ') || self.chr0 == Some('\t')
                    || self.chr0 == Some('\x0C') || self.chr0 == Some('\n')
                {
                    self.next_char();
                }
            }
            '\\' => {
                self.next_char();
                if let Some('\n') = self.chr0 {
                    self.next_char();
                } else {
                    return Err(LexicalError {
                        error: LexicalErrorType::LineContinuationError,
                        location: self.get_pos(),
                    });
                }
            }

            _ => {
                let c = self.next_char();
                return Err(LexicalError {
                    error: LexicalErrorType::UnrecognizedToken { tok: c.unwrap() },
                    location: self.get_pos(),
                });
            }
        }

        Ok(())
    }

    fn eat_single_char(&mut self, ty: Tok) {
        let tok_start = self.get_pos();
        self.next_char().unwrap();
        let tok_end = self.get_pos();
        self.emit((tok_start, ty, tok_end));
    }

    /// Helper function to go to the next character coming up.
    fn next_char(&mut self) -> Option<char> {
        let c = self.chr0;
        let nxt = self.chars.next();
        self.chr0 = self.chr1;
        self.chr1 = self.chr2;
        self.chr2 = nxt;
        if c == Some('\n') {
            self.location.newline();
        } else {
            self.location.go_right();
        }
        c
    }

    /// Helper function to retrieve the current position.
    fn get_pos(&self) -> Location {
        self.location.clone()
    }

    /// Helper function to emit a lexed token to the queue of tokens.
    fn emit(&mut self, spanned: Spanned) {
        self.pending.push(spanned);
    }
}

/* Implement iterator pattern for the get_tok function.
Calling the next element in the iterator will yield the next lexical
token.
 */
impl<T> Iterator for Lexer<T>
where
    T: Iterator<Item = char>,
{
    type Item = LexResult;

    fn next(&mut self) -> Option<Self::Item> {
        // Idea: create some sort of hash map for single char tokens:
        // let mut X = HashMap::new();
        // X.insert('=', Tok::Equal);
        let token = self.inner_next();
        // trace!(
        //     "Lex token {:?}, nesting={:?}, indent stack: {:?}",
        //     token,
        //     self.nesting,
        //     self.indentation_stack
        // );

        match token {
            Ok((_, Tok::EndOfFile, _)) => None,
            r => Some(r),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    pub fn lex_source(source: &str) -> Vec<Tok> {
        let lexer = make_tokenizer(source);
        lexer.map(|x| x.unwrap().1).collect()
    }

    #[test]
    fn test_numbers() {
        let source = "0x2f 0b1101 0   123 0.2";
        let tokens = lex_source(source);
        assert_eq!(
            tokens,
            vec![
                Tok::Int(47),
                Tok::Int(13),
                Tok::Int(0),
                Tok::Int(123),
                Tok::Double(0.2),
            ]
        );
    }

    macro_rules! test_line_comment {
        ($($name:ident: $eol:expr,)*) => {
            $(
                #[test]
                fn $name() {
                    let source = format!(r"99232  // {}", $eol);
                    let tokens = lex_source(&source);
                    assert_eq!(tokens, vec![Tok::Int(99232)]);
                }
            )*
        }
    }

    test_line_comment! {
        test_line_comment_long: " foo",
        test_line_comment_whitespace: "  ",
        test_line_comment_single_whitespace: " ",
        test_line_comment_empty: "",
    }

    #[test]
    fn test_line_comment_until_eol() {
        let tokens = lex_source("123  // Foo 22\n456");
        assert_eq!(
            tokens,
            vec![
                Tok::Int(123),
                Tok::Int(456),
            ]
        )
    }

    #[test]
    fn test_parse_keyword() {
        let tokens = lex_source("node struct");
        assert_eq!(
            tokens,
            vec![Tok::Node, Tok::Struct]
        )
    }

    #[test]
    fn test_parse_string() {
        let tokens = lex_source(" \"hello there\" {\"hi\", \"you\"} ");
        assert_eq!(
            tokens,
            vec![Tok::String("hello there".to_owned()),
                 Tok::Lbrace,
                 Tok::String("hi".to_owned()),
                 Tok::Comma,
                 Tok::String("you".to_owned()),
                 Tok::Rbrace,
            ]
        )
    }

    #[test]
    fn test_parse_name() {
        let tokens = lex_source("node foo { Stuff }");
        assert_eq!(
            tokens,
            vec![Tok::Node,
                 Tok::Name("foo".to_owned()),
                 Tok::Lbrace,
                 Tok::Name("Stuff".to_owned()),
                 Tok::Rbrace,
            ]
        )
    }

    #[test]
    fn test_parse_dotdot() {
        let tokens = lex_source("0 .. 5.");
        assert_eq!(tokens, vec![Tok::Int(0), Tok::DotDot, Tok::Double(5.0)]);
        let tokens = lex_source("0..5");
        assert_eq!(tokens, vec![Tok::Int(0), Tok::DotDot, Tok::Int(5)]);
    }
}
