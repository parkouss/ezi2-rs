//! Implement abstract syntax tree (AST) nodes for the ezi language.

pub use crate::location::Location;
use std::collections::HashMap;

pub type DottedName = Vec<String>;
pub type Bases = Vec<DottedName>;

#[derive(Debug, PartialEq)]
pub struct Module {
    pub imports: Vec<String>,
    pub definitions: Vec<ModuleDef>,
}

#[derive(Debug, PartialEq)]
pub enum ModuleDef {
    Struct(Struct),
    Enum(Enum),
    Variant(Variant),
    Node(Node),
}

#[derive(Debug, PartialEq)]
pub struct Enum {
    pub location: Location,
    pub docstring: Option<String>,
    pub name: String,
    pub values: Vec<String>,
}

#[derive(Debug, PartialEq)]
pub struct Variant {
    pub location: Location,
    pub docstring: Option<String>,
    pub name: String,
    pub fields: Vec<Field>,
}

#[derive(Debug, PartialEq)]
pub struct Struct {
    pub location: Location,
    pub docstring: Option<String>,
    pub name: String,
    pub bases: Option<Bases>,
    pub definitions: Vec<StructDef>,
    pub properties: Option<Properties>,
}

#[derive(Debug, PartialEq)]
pub enum StructDef {
    Field(Field),
    Struct(Struct),
    Enum(Enum),
    Variant(Variant),
}

#[derive(Debug, PartialEq)]
pub struct Field {
    pub location: Location,
    pub docstring: Option<String>,
    pub ftype: FieldType,
    pub name: String,
}

#[derive(Debug, PartialEq)]
pub enum FieldType {
    Int {constraints: Option<Vec<Constraint>>},
    Uint {constraints: Option<Vec<Constraint>>},
    Bool,
    String,
    Double {constraints: Option<Vec<Constraint>>},
    Ref(DottedName),
    List {t: Box<FieldType>, constraints: Option<Vec<Constraint>>},
    Map {t: Box<FieldType>, constraints: Option<Vec<Constraint>>},
    Optional {t: Box<FieldType>},
    RefValue {t: Box<FieldType>},
}

#[derive(Debug, PartialEq)]
pub struct Node {
    pub location: Location,
    pub docstring: Option<String>,
    pub name: String,
    pub bases: Option<Bases>,
    pub is_interface: bool,
    pub definitions: Vec<NodeDef>,
    pub params: Option<Params>,
}

#[derive(Debug, PartialEq)]
pub enum NodeDef {
    Struct(Struct),
    Enum(Enum),
    Variant(Variant),
}

#[derive(Debug, PartialEq)]
pub struct Params {
    pub bases: Option<Bases>,
    pub fields: Vec<Field>,
    pub public_ref: Vec<DottedName>,
    pub properties: Option<Properties>,
}

#[derive(Debug, PartialEq)]
pub enum PropertyValue {
    Bool(bool),
}
pub type Properties = HashMap<String, PropertyValue>;

#[derive(Debug, PartialEq)]
pub struct Constraint {
    pub name: String,
    pub value: ConstraintValue,
}

#[derive(Debug, PartialEq)]
pub enum ConstraintValue {
    Range(Option<Number>, Option<Number>),
    Number(Number),
}

#[derive(Debug, PartialEq)]
pub enum Number { Int(i64), Double(f64) }
