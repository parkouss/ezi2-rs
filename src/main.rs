mod location;
mod token;
mod error;
mod lexer;
mod ast;

use lalrpop_util::lalrpop_mod;
lalrpop_mod!(ezi);

use std::io::{self, Read};

pub fn parse(source: &str) -> Result<ast::Module, error::ParseError> {
    let lxr = lexer::make_tokenizer(source);
    ezi::ModuleParser::new().parse(lxr)
}

fn main() {
    let mut buffer = String::new();
    let stdin = io::stdin();
    let mut handle = stdin.lock();

    handle.read_to_string(&mut buffer).unwrap();
    println!("{:?}", parse(&buffer));

//     let res = parse("
// import foo;
// import bar;

// \"this is some\"
// \" doc\"
// struct Foo { int toto; }

// enum E1 { a,b }

// struct Stuff : Foo {
//   properties { customValidate: true }

//   struct What { string str; }
//   list<uint> lala;
//   refvalue <int> rv;
// }

// variant V1 {
//   int f;
//   string t;
//   Stuff stuff;
//   foo.Foo foo;
// }

// \"some doc\"
// interface Interface {
//   struct Stuff { V1 v1; }
// }

// node Node : Interface {
//    params {
//      int f;
//      foo.Foo lala;
//      public_ref: stuff, vehicle.id;
//    }
// }

// ");
//     println!("{:?}", res);
}
