//! Define internal parse error types
//! The goal is to provide a matching and a safe error API, maksing errors from LALR
use lalrpop_util::ParseError as LalrpopError;

use crate::location::Location;
use crate::token::Tok;

use std::fmt;

/// Represents an error during lexical scanning.
#[derive(Debug, PartialEq)]
pub struct LexicalError {
    pub error: LexicalErrorType,
    pub location: Location,
}

#[derive(Debug, PartialEq)]
pub enum LexicalErrorType {
    UnrecognizedToken { tok: char },
    StringError,
    NestingError,
    LineContinuationError,
    OtherError(String),
}

impl fmt::Display for LexicalErrorType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            LexicalErrorType::UnrecognizedToken { tok } => {
                write!(f, "Got unexpected token {}", tok)
            }
            LexicalErrorType::StringError => write!(f, "String error"),
            LexicalErrorType::NestingError => write!(f, "Nesting error"),
            LexicalErrorType::LineContinuationError => write!(f, "Line continuation error"),
            LexicalErrorType::OtherError(msg) => write!(f, "{}", msg),
        }
    }
}


/// Represents an error during parsing
pub type ParseError = LalrpopError<Location, Tok, LexicalError>;

impl From<LexicalError> for ParseError {
    fn from(err: LexicalError) -> Self {
        lalrpop_util::ParseError::User { error: err }
    }
}
